/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import SomChbabDate from './SomchbabDate';
import { ApplicationProvider, IconRegistry, Layout, Text } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { mapping, light as lightTheme,dark as darkTheme } from '@eva-design/eva';
import React from 'react';
const App1 = () => (
    <React.Fragment>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <SomChbabDate />
      </ApplicationProvider>
    </React.Fragment>
  );
AppRegistry.registerComponent(appName, () => App1);
