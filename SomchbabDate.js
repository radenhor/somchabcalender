import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    TextInput,
    AsyncStorage,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    TabBarIOS,
    Alert,
} from 'react-native';
import CheckBox from 'react-native-check-box'
class SomchbabDate extends React.Component {
    constructor(props) {
        super(props)
        this.months = ["January", "February", "March", "April",
            "May", "June", "July", "August", "September", "October",
            "November", "December"];

        this.weekDays = [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
        ];
        this.nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        this.future = new Date();
        this.state = {
            activeDate: new Date(),
            checkedItems: new Map(),
            startDay: 0,
            startDayShift: '',
            endDayShift: '',
            endDay: 0,
            permissionCount : 0,
        }
        this.matrix = []
    }
    countPermission = () => {
        var count = 0;
        if(this.state.endDay!=0){
            for(var i=this.state.startDay;i<=this.state.endDay;i++){
                var myDate = new Date();
                myDate.setFullYear(this.state.activeDate.getFullYear());
                myDate.setMonth(this.state.activeDate.getMonth());
                myDate.setDate(i);
                if(myDate.getDay() != 6 && myDate.getDay() != 0){
                    if(this.state.checkedItems.get('am'+i)==true){
                        count++;
                    }
                    if(this.state.checkedItems.get('pm'+i)==true){
                        count++;
                    }
                }
            }
        }else{
            if(this.state.startDay!=0){
                count++;
            }
        }
        this.setState({
            permissionCount : count
        })
    }
    generateMatrix = () => {
        var year = this.state.activeDate.getFullYear();
        var month = this.state.activeDate.getMonth();
        var firstDay = new Date(year, month, 1).getDay();
        var maxDays = this.nDays[month];
        if (month == 1) { // February
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
                maxDays += 1;
            }
        }
        // Create header
        this.matrix[0] = this.weekDays;
        var counter = 1;
        for (var row = 1; row < 7; row++) {
            this.matrix[row] = [];
            for (var col = 0; col < 7; col++) {
                this.matrix[row][col] = -1;
                if (row == 1 && col >= firstDay) {
                    // Fill in rows only after the first day of the month
                    this.matrix[row][col] = counter++;
                } else if (row > 1 && counter <= maxDays) {
                    // Fill in rows only if the counter's not greater than
                    // the number of days in the month
                    this.matrix[row][col] = counter++;
                }
            }
        }
        return this.matrix;
    }
    changeMonth = (n) => {
        
        this.setState(() => {
            this.state.activeDate.setMonth(
                this.state.activeDate.getMonth() + n
            )
            this.setState({
                checkedItems: new Map(),
                startDay: 0,
                startDayShift: '',
                endDayShift: '',
                endDay: 0,
                permissionCount : 0
            })
            return this.state;
        });
    }
    onAmSelect = (shift, day, col) => {
        if (this.state.checkedItems.get(shift + day) != true) {
            if (this.state.startDay == 0 && this.state.endDay == 0) {
                this.setState({
                    startDay: day,
                    startDayShift: shift,
                    checkedItems: this.state.checkedItems.set(shift + day, true)
                })
            } else if (this.state.startDay != 0 && this.state.endDay == 0) {
                if (day > this.state.startDay) {
                    this.setState({
                        ...this.state,
                        endDay: day,
                        endDayShift: shift
                    })
                    for (var i = this.state.startDay; i <= day; i++) {
                        if (i == this.state.startDay) {
                            if (this.state.startDayShift == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('am' + i, true),
                                })
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('pm' + i, true),
                                })
                            }
                        } else if (i == day) {
                            if (this.state.startDay == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`am${i}`, true),
                                })
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`pm${i}`, true),
                                })
                            } else {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`am${i}`, true),
                                })
                            }
                        } else {
                            this.setState({
                                checkedItems: this.state.checkedItems.set(`am${i}`, true),
                            })
                            this.setState({
                                checkedItems: this.state.checkedItems.set(`pm${i}`, true),
                            })
                        }
                    }
                }
            } else {
                if (day > this.state.startDay) {
                    this.setState({
                        ...this.state,
                        endDay: day,
                        endDayShift: shift
                    })
                    for (var i = this.state.startDay; i <= day; i++) {
                        if (i == this.state.startDay) {
                            if (this.state.startDayShift == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('am' + i, true),
                                })
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('pm' + i, true),
                                })
                            }
                        } else if (i == day) {
                            if (this.state.startDay == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`am${i}`, true),
                                })
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`pm${i}`, true),
                                })
                            } else {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set(`am${i}`, true),
                                })
                            }
                        } else {
                            this.setState({
                                checkedItems: this.state.checkedItems.set(`am${i}`, true),
                            })
                            this.setState({
                                checkedItems: this.state.checkedItems.set(`pm${i}`, true),
                            })
                        }
                    }
                }
            }
        } else {
            if (day == this.state.startDay) {
                for (var i = day; i <= this.state.endDay; i++) {
                    this.setState({
                        checkedItems: this.state.checkedItems.set('am' + i, false),
                    })
                    this.setState({
                        checkedItems: this.state.checkedItems.set('pm' + i, false),
                    })
                }
                this.setState({
                    endDay: 0,
                    startDay: 0,
                    startDayShift: '',
                    endDayShift: ''
                }, () => {
                    this.setState({
                        checkedItems: this.state.checkedItems.set('am' + day, false),
                    })
                })
            } else if (day == this.state.endDay) {
                if (this.state.endDayShift == 'pm') {
                    this.setState({
                        endDayShift: 'pm',
                        checkedItems: this.state.checkedItems.set('am' + day, false),
                    })
                    this.setState({
                        checkedItems: this.state.checkedItems.set('pm' + day, false),
                        endDay: JSON.stringify(col) == "1" ? day - 3 : day -1,
                    })
                } else {
                    this.setState({
                        endDayShift: 'pm',
                        endDay: JSON.stringify(col) == "1" ? day - 3 : day -1,
                        checkedItems: this.state.checkedItems.set(shift + day, false),
                    })
                }
                
            } else {
                for (var i = day; i <= this.state.endDay; i++) {
                    this.setState({
                        endDayShift : 'pm',
                        checkedItems: this.state.checkedItems.set('am' + i, false),
                    })
                    this.setState({
                        checkedItems: this.state.checkedItems.set('pm' + i, false),
                        endDayShift : 'pm',
                        endDay: JSON.stringify(col) == "1" ? day - 3 : day -1,
                    })
                }
                
            }
        }
    }
    onPmSelect = (shift, day, col) => {
        if (this.state.checkedItems.get(shift + day) != true) {
            if (this.state.startDay == 0 && this.state.endDay == 0) {
                this.setState({
                    startDay: day,
                    startDayShift: shift,
                    checkedItems: this.state.checkedItems.set(shift + day, true)
                })
            } else if (this.state.startDay != 0 && this.state.endDay == 0) {
                if (day >= this.state.startDay) {
                    this.setState({
                        endDay: day,
                        endDayShift: shift
                    })
                    for (var i = this.state.startDay; i <= day; i++) {
                        if (i == this.state.startDay) {
                            if (this.state.startDayShift == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('pm' + i, true)
                                })
                            }
                        } else {
                            this.setState({
                                checkedItems: this.state.checkedItems.set('pm' + i, true)
                            })
                            this.setState({
                                checkedItems: this.state.checkedItems.set('am' + i, true)
                            })
                        }
                    }
                }
            } else {
                if (day > this.state.startDay) {
                    this.setState({
                        endDay: day,
                        endDayShift: shift
                    })
                    for (var i = this.state.startDay; i <= day; i++) {
                        if (i == this.state.startDay) {
                            if (this.state.startDayShift == 'am') {
                                this.setState({
                                    checkedItems: this.state.checkedItems.set('pm' + i, true)
                                })
                            }
                        } else {
                            this.setState({
                                checkedItems: this.state.checkedItems.set('pm' + i, true)
                            })
                            this.setState({
                                checkedItems: this.state.checkedItems.set('am' + i, true)
                            })
                        }
                    }
                }
            }
        } else {
            if (day == this.state.startDay) {
                if(this.state.endDay==0){
                    this.setState({
                        endDay: 0,
                        startDay: 0,
                        startDayShift: '',
                        endDayShift: ''
                    }, () => {
                        this.setState({
                            checkedItems: this.state.checkedItems.set('pm' + day, false),
                        })
                    })
                }
                var b = true
                for (var i = day; i <= this.state.endDay; i++) {
                    if(this.state.startDayShift=='am'&&day==this.state.endDay){
                        
                        this.setState({
                            checkedItems: this.state.checkedItems.set('pm' + i, false),
                        })
                        this.setState({
                            endDay: 0,
                            startDayShift : 'am',
                            endDayShift: ''
                        })
                    }else{
                        if(this.state.startDayShift=='am'&&i==day){
                            this.setState({
                                checkedItems: this.state.checkedItems.set('pm' + i, false),
                            })
                            b = false
                        }else{
                            this.setState({
                                checkedItems: this.state.checkedItems.set('pm' + i, false),
                            })
                            this.setState({
                                checkedItems: this.state.checkedItems.set('am' + i, false),
                            })
                            
                        }
                        if(b==true){
                            this.setState({
                                endDay: 0,
                                startDay : 0,
                                startDayShift : '',
                                endDayShift: ''
                            })
                        }else{
                            this.setState({
                                endDay: 0,
                                startDay : day,
                                startDayShift : 'am',
                                endDayShift: 'am'
                            })
                        }
                    }
                }
            } else if (day == this.state.endDay) {
                if (this.state.endDayShift == 'pm') {
                    this.setState({
                        endDayShift: 'am',
                        checkedItems: this.state.checkedItems.set('pm' + day, false),
                    })
                }
            } else {
                for (var i = day; i <= this.state.endDay; i++) {
                    if (day == i) {
                        this.setState({
                            checkedItems: this.state.checkedItems.set('pm' + i, false),
                        })
                    } else {
                        this.setState({
                            checkedItems: this.state.checkedItems.set('am' + i, false),
                        })
                        this.setState({
                            checkedItems: this.state.checkedItems.set('pm' + i, false),
                        })
                    }
                }
                this.setState({
                    endDay: day,
                    endDayShift: 'am'
                })
            }
        }
        console.log("PM" + shift + day)
    }
    calenderView = () => {
        this.matrix = this.generateMatrix()
        let activeDate = this.state.activeDate
        let nowDate = new Date()
        var rows = [];
        rows = this.matrix.map((row, rowIndex) => {
            var rowItems = row.map((item, colIndex) => {
                return (
                    <View style={{
                        flex: 1,
                        height: 18,

                    }}>
                        {item != -1 ?
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{
                                    alignSelf: 'center', textAlign: 'center',
                                    // Highlight header
                                    backgroundColor: rowIndex == 0 ? '#ddd' : '#fff',
                                    // Highlight Sundays
                                    color: colIndex == 0 || colIndex == 6 ? '#a00' : '#000',
                                    // Highlight current date
                                    fontWeight: item == this.state.activeDate.getDate()
                                        ? 'bold' : ''
                                }}>
                                    {item}
                                </Text>
                                {rowIndex==0 && 
                                <View style={{ flexDirection: 'row',justifyContent:"space-around" }}>
                                    <Text style={{alignSelf:'center'}}>M</Text>
                                    <Text style={{alignSelf:'center'}}>A</Text>
                                </View>
                                }
                                {((colIndex != 6 && colIndex != 0) && (rowIndex != 0)) &&
                                    <View style={{ flexDirection: 'column'}}>
                                        <View style={{ flexDirection: 'row',justifyContent:'space-around'}}>
                                            <TouchableHighlight>
                                                <View style={{ flexDirection: 'column'}}>
                                                    <CheckBox disabled = {activeDate.getFullYear()<nowDate.getFullYear()?true:activeDate.getMonth()<nowDate.getMonth()?true:false}
                                                        onClick={() => this.onAmSelect("am", item, colIndex)}
                                                        isChecked={this.state.checkedItems.get("am" + item)}
                                                        style={{borderColor: '#FFFFFF'}}
                                                        name={"am" + item} />
                                                    
                                                </View>
                                            </TouchableHighlight>
                                            <TouchableHighlight>
                                                <View style={{ flexDirection: 'column'}}>
                                                    <CheckBox disabled = {activeDate.getFullYear()<nowDate.getFullYear()?true:activeDate.getMonth()<nowDate.getMonth()?true:false}
                                                        onClick={() => this.onPmSelect("pm", item, colIndex)}
                                                        isChecked={this.state.checkedItems.get("pm" + item)}
                                                        name={"pm" + item}
                                                    />
                                                </View>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                }

                            </View>
                            : null}

                    </View>

                );
            });
            return (
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        padding: 20,
                        justifyContent: 'space-around',
                        alignItems: 'center',
                    }}>
                    {rowItems}
                </View>
            );
        });
        return rows
    }
    
    render() {
        return (
            <SafeAreaView somchbab = {this.state}>
                {<this.calenderView />}
                <View style={{flexDirection:'row',justifyContent:'center'}}>
                    <Button title='Prev' onPress={() => this.changeMonth(-1)}></Button>
                    <Button title='Next' onPress={() => this.changeMonth(+1)}></Button>
                </View>
                {
                    this.state.startDay == 0 && this.state.endDay == 0 
                    ?
                    <View>
                        <Text>Start Day : dd/------/yyyy</Text>
                        <Text>End Day : dd/------/yyyy</Text>
                    </View>
                     :
                     <View>
                        <Text>Start Day : {this.state.activeDate.getFullYear()}/{this.state.activeDate.getMonth()+1}/{this.state.startDay==0?this.state.activeDate.getDate():this.state.startDay} Shift {this.state.startDayShift}</Text>
                        <Text>End Day : {this.state.activeDate.getFullYear()}/{this.state.activeDate.getMonth()+1}/{this.state.endDay==0?this.state.startDay==0?this.state.activeDate.getDate():this.state.startDay:this.state.endDay} Shift {this.state.endDayShift}</Text>
                    </View>
                }       
                <Text>Permission Count : {this.state.permissionCount}</Text>
                <Button title='Count' onPress={() => this.countPermission()}></Button>
            </SafeAreaView>
        )
    }
}
export default SomchbabDate;