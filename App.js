/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TextInput,
  AsyncStorage,
  Image
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import ImagePicker from 'react-native-image-picker';
import SocketCient from 'socket.io-client'
import axios from 'axios'
import OneSignal from 'react-native-onesignal';
import ImagePicker from 'react-native-image-crop-picker';
class App extends React.Component{
  constructor(props){
    super(props);
    OneSignal.init("78feeaa4-1dde-4409-ade8-102e2ffac204");
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.addEventListener('opened', this.onOpened);
    this.state = {
      email : '',
      password : '',
      notification : 'none',
      user : null,
      image : [],
      uri : ''
    }
    this.socket = SocketCient('http://10.0.2.2:9000/hrdapp',{ 
        reconnection: true,
        reconnectionDelay: 500,
        jsonp: false,
        reconnectionAttempts: Infinity,
        transports: ['websocket']});
    this.socket.on('connect_error', (err) => {
      
    });
    this.socket.on('connect',()=>{
      AsyncStorage.getItem('user').then((user)=>{
        if(user!=null){
          console.log(JSON.parse(user).authorities[0].role)
          if(JSON.parse(user).authorities[0].role=='STUDENT'){
            axios({
              method :'GET',
              url:`http://10.0.2.2:8080/api/v1/student/find-generation-by-student-id/${JSON.parse(user).id}`,
              headers : {Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='}
            }).then((res)=>{
              if(res.data.data.length>0){
                res.data.data.map((g)=>{
                  this.socket.emit('joinRoom','postToStudent'+g.id,()=>{
                    this.socket.on('postToStudent'+g.id,(data)=>{
                        this.setState({
                          notification : data
                        })
                    })
                  })
                })
                
              }
            })
          }else if(JSON.parse(user).authorities[0].role=='TEACHER'){
            this.socket.on('postToTeacher',(data)=>{
              console.log(data)
              this.setState({
                notification:'postToTeacher'
              })
            })
          }
          
          console.log(JSON.parse(user))
          this.socket.emit("connectToServer",JSON.parse(user).id)
          console.log(JSON.parse(user).id)
          this.setState({
            user : JSON.parse(user)
          })  
          this.socket.on('requestPermission', (data) => {
            console.log(data)
            this.setState({
              notification : 'request from Mr raden'
            })
            this.forceUpdate()
            // console.log("DATA",data)
          });
          this.socket.on('approve'+this.state.user.id,(data)=>{
            this.setState({
              notification : 'aprove'
            })
          })
          this.socket.on('reject'+this.state.user.id,(data)=>{
            this.setState({
              notification : 'reject'
            })
          })
        
      }  
      })
    })
    this.socket.on('post',(data)=>{
        this.setState({
          notification : 'post'
        })
    })
    
  }
  options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  onHiClick = ()=>{
    let data = {
      id: 0,
      dateFrom: "2019-12-30",
      dateTo: "2019-12-31",
      leaveStatus: "p",
      teacherResponseStatus: "p",
      adminResponseStatus: "p",
      leaveTime: "string",
      arriveTime: "string",
      duration: 0,
      amPm: "AM",
      permissionCount: 0,
      reason: "Sick Aff",
      deleted: true,
      status: "p",
      createdAt: "2019-12-30T02:21:18.439Z",
      classenroll: {
        id: 23,
        classroom: null,
        user: {
          id: this.state.user.id,
          name: "string",
          email: "string",
          role: "string"
        },
        status: true,
        deleted: true
      }
    }
    axios({
      method : 'POST',
      url : 'http://10.0.2.2:8080/api/v1/student/request-permission',
      headers : {Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='},
      data : data
    }).then((res)=>{
      console.log(res.data.data)
      //  this.socket.emit('requestPermission',res.data.data)
    }).catch(err=>console.log(err))
    
  }
  onLogin = ()=>{
    let userLogin = new FormData();
    userLogin.append('email',this.state.email)
    userLogin.append('password',this.state.password)
    console.log(this.state)
      axios({
        method : 'POST',
        url : 'http://10.0.2.2:8080/login',
        data : userLogin,
      }).then((res)=>{
        AsyncStorage.setItem('user',JSON.stringify(res.data))
        console.log(res.data)
      }).catch((err)=>console.log(err))
  }
  onPost = () =>{
    let data = {
      id: 0,
      title: "Test",
      description: "string",
      status: true,
      createdDate: "2019-12-30T08:02:00.581Z",
      privacy: {
        id: 1,
        privacy: "Public",
        status: true
      },
      category: {
        id: 1,
        category: "Schoolar Ship",
        status: true
      },
      image: [
        {
          id: 0,
          postId: 0,
          url: "string",
          thumbnail: true
        }
      ]
    }
    axios({
      method : 'POST',
      url : 'http://10.0.2.2:8080/api/v1/admin/news',
      headers : {Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='},
      data : data
    }).then((res)=>{
      this.socket.emit('post',res.data.data)
    })
  }

  onIds(device) {
    console.log('Device info: ', device);
    AsyncStorage.getItem('user').then((user)=>{
      if(user!=null){
          axios({
            method:'PUT',
            url : `http://10.0.2.2:8080/api/v1/user/subscribe-notification/${user.id}/${device.userId}`,
            headers : {Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='},
          }).then((res)=>console.log(res)).catch((res)=>console.log('Err12',res))
      }
    })
  }
  onOpened(openResult) {
    if(openResult.action.actionID=='btnAccept'){
      console.log("Accept")
    }else if(openResult.action.actionID=='btnReject'){
      console.log("Reject")
    }else{
      console.log("Simple")
    }
  }
  onPhoto = () => {
    ImagePicker.showImagePicker(this.options, (response) => {
      // console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          uri : response.uri
        })
        this.state.image.unshift(response)
        this.forceUpdate()
      }
    });
  }
  remove = (index) => {
    this.state.image.splice(index,1)
    this.forceUpdate()
  }
  upload = () => {
    // axios({
    //   method : 'GET',
    //   url : 'http://10.0.2.2:8080/api/v1/admin/news',
    //   headers : {Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='},
      
    // }).then((res)=>{
    //   console.log(res)
    // })
    let size = 0;
    const formData = new FormData();
    this.state.image.map((file)=>{
      size = size + (file.size/1000000)
      formData.append('files', {
        name : "image.png",
        type : "image/png",
        uri : file.path
      });
    })
    console.log("TOTAL",size)
    axios({
      method : 'POST',
      url : 'http://rabbitboy.me:15003/api/v1/admin/uploadfile/multi',
      headers : {'Content-Type': 'application/x-www-form-urlencoded',Authorization : 'Basic cG9sZW5zb2tAZ21haWwuY29tOjEyMw=='},
      data : formData
    }).then((res)=>{
      console.log(res.data)
      console.log(res.data.data.length)
    }).catch(err=>console.log("Error"+err))
  }
  uploadMulti = () => {
    ImagePicker.openPicker({
      multiple: true
    }).then(images => {
      images.map((file)=>{
        this.state.image.push(file)
      })
      this.forceUpdate()
    });
  }
  hi = ()=>{
    this.socket.emit('Hello','Hi')
  }
  disconnect = ()=>{
    this.socket.emit('disconnectFromServer')
  }
  render(){
    console.log(this.state.uri)
    return(
      <SafeAreaView>
        <ScrollView>

      
        <TextInput style={{borderColor:'black',borderRadius:1,width:300}} onChangeText={(email)=>{
          this.setState({email:email})
        }} name='email' value={this.state.email} placeholder='Email'></TextInput>
        <TextInput style={{borderColor:'black',borderRadius:1,width:300}} name='password' onChangeText={(password)=>{
          this.setState({password:password})
        }} value={this.state.password} placeholder='Password' ></TextInput>
        <Text>Bagde : {this.state.notification}</Text>
        <Button title='Login' onPress={()=>this.onLogin()}></Button>
        <Button title='Hi' onPress={()=>this.hi()}></Button>
        <Button title='Request Permision' onPress={()=>this.onHiClick()}></Button>
        <Button title='Post News' onPress={()=>this.onPost()}></Button>
        <Button title='add photo' onPress={()=>this.onPhoto()}></Button>
        <Button title='add photo' onPress={()=>this.uploadMulti()}></Button>
        <Button title='Disconnect' onPress={()=>this.disconnect()}></Button>
        {/* <Image  style={{marginTop:10,borderRadius:50,width:100,height:100,alignSelf:'center',marginBottom:10}} source={{uri:this.state.uri}}></Image> */}
        
        {
          this.state.image.map((res,index)=>{
            // console.log(res.uri)
            return(
              <View style={{flexDirection:'row'}}>
                  <Button title="remove" onPress={()=>this.remove(index)}></Button>
                  <Image  style={{marginTop:10,borderRadius:50,width:100,height:100,alignSelf:'center',marginBottom:10}} source={{uri:res.path}}></Image>
              </View>
            )
          })
        }
        <Button title='Upload' onPress={()=>this.upload()}></Button>
        </ScrollView>
      </SafeAreaView>
    )
  }
}
export default App;
